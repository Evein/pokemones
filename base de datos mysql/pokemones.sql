-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-11-2020 a las 00:01:56
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pokemones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE `historial` (
  `id_historial` int(11) NOT NULL,
  `nombre` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`id_historial`, `nombre`) VALUES
(1, '0'),
(2, '0'),
(3, 'metapod');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nuevospok`
--

CREATE TABLE `nuevospok` (
  `id_nuevopok` int(10) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `movimiento` varchar(15) NOT NULL,
  `habilidad` varchar(15) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `imagen` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nuevospok`
--

INSERT INTO `nuevospok` (`id_nuevopok`, `nombre`, `movimiento`, `habilidad`, `tipo`, `imagen`) VALUES
(1, 'pokeroca', 'piedra fuerte', 'lanzar', 'roca', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrantes`
--

CREATE TABLE `registrantes` (
  `id_registrante` int(10) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `contra` varchar(15) NOT NULL,
  `correo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `registrantes`
--

INSERT INTO `registrantes` (`id_registrante`, `usuario`, `contra`, `correo`) VALUES
(1, 'adan', '123', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `historial`
--
ALTER TABLE `historial`
  ADD PRIMARY KEY (`id_historial`);

--
-- Indices de la tabla `nuevospok`
--
ALTER TABLE `nuevospok`
  ADD PRIMARY KEY (`id_nuevopok`);

--
-- Indices de la tabla `registrantes`
--
ALTER TABLE `registrantes`
  ADD PRIMARY KEY (`id_registrante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historial`
--
ALTER TABLE `historial`
  MODIFY `id_historial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nuevospok`
--
ALTER TABLE `nuevospok`
  MODIFY `id_nuevopok` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `registrantes`
--
ALTER TABLE `registrantes`
  MODIFY `id_registrante` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
