<?php
include 'const.php';
session_start();
if(isset($_POST['deslogueo'])){
		session_destroy();
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Pokeinformacion</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/">

    <!-- Bootstrap core CSS -->
<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
  </head>
  <body>
    <header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Pokeinformacion</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="registro.php">Registro </a>
        </li>
		<li class="nav-item active">
          <a class="nav-link" href="sesion.php">Log in <span class="sr-only">(current)</span></a>
        </li>
	  <?php
if(isset($_SESSION['login'])){
?>		
		<li class="nav-item active">
           <form action="index.php" method="post" class="form-inline mt-2 mt-md-0">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="deslogueo">Log out</button>
		</form>
        </li>
      </ul>

 <li class="nav-item active">
          <a class="nav-link" href="sesion.php">Bienvenido <?php echo  $_SESSION['login'] ?> <span class="sr-only">(current)</span></a>
        </li>
		<li   class="nav-item active">
          <a  class="nav-link" href="registropok.php">Crear pokemon  <span class="sr-only">(current)</span></a>
        </li>
<?php
}
?>
	 
      <form action="index.php" method="post" class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Buscar pokemon" aria-label="Buscar" type="text"  name="busqueda">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="envia">Buscar</button>
		</form>
    </div>
  </nav>
</header>

<main role="main">




  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
    
<div class="row">

    <?php
	if(isset($_POST['busqueda'])){
		$npok=$_POST['busqueda'];
	busqueda($npok);
}else{
	pokemones(); 
	pokemonesdb();
	}
	?>
 	</div><!-- /.row -->
  </div><!-- /.container -->

  <!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2017-2020 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
  </footer>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
</html>
